<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $employees=[
            [
            'name' => 'Noel Carroll',
			'value1' => 429,
			'value2' => 329,
			'value3' => 427,
			'value4' => 426,
			'value5' => 459
            ],
            [
            'name' => 'Elise Lyon',
			'value1' => 429,
			'value2' => 329,
			'value3' => 527,
			'value4' => 436,
			'value5' => 459
            ],
            [
            'name' => 'Geraldine Wiggins',
			'value1' => 429,
			'value2' => 329,
			'value3' => 427,
			'value4' => 426,
			'value5' => 459
            ],
            [
            'name' => 'Noel Carroll',
			'value1' => 429,
			'value2' => 329,
			'value3' => 427,
			'value4' => 426,
			'value5' => 459
            ],
            [
            'name' => 'Noel Carroll',
			'value1' => 429,
			'value2' => 329,
			'value3' => 427,
			'value4' => 426,
			'value5' => 459
            ],          
            [
            'name' => 'Federico Pena',
			'value1' => 429,
			'value2' => 329,
			'value3' => 427,
			'value4' => 426,
			'value5' => 459
            ],
            [
            'name' => 'Noel Carroll',
			'value1' => 429,
			'value2' => 329,
			'value3' => 427,
			'value4' => 426,
			'value5' => 459
            ],
            [
            'name' => 'Federico Pena',
			'value1' => 429,
			'value2' => 329,
			'value3' => 427,
			'value4' => 426,
			'value5' => 459
            ],
            [
            'name' => 'Noel Carroll',
			'value1' => 429,
			'value2' => 329,
			'value3' => 427,
			'value4' => 426,
			'value5' => 459
            ],
            [
            'name' => 'Noel Carroll',
			'value1' => 429,
			'value2' => 329,
			'value3' => 427,
			'value4' => 426,
			'value5' => 459
            ],
            [
            'name' => 'Federico Pena',
			'value1' => 429,
			'value2' => 329,
			'value3' => 427,
			'value4' => 426,
			'value5' => 459
            ],
            [
            'name' => 'Noel Carroll',
			'value1' => 429,
			'value2' => 329,
			'value3' => 427,
			'value4' => 426,
			'value5' => 459
            ],
            [
            'name' => 'Federico Pena',
			'value1' => 429,
			'value2' => 329,
			'value3' => 427,
			'value4' => 426,
			'value5' => 459
            ],
            [
            'name' => 'Noel Carroll',
			'value1' => 429,
			'value2' => 329,
			'value3' => 487,
			'value4' => 426,
			'value5' => 489
            ],
            [
            'name' => 'Federico Pena',
			'value1' => 429,
			'value2' => 329,
			'value3' => 487,
			'value4' => 426,
			'value5' => 489
            ],
            [
            'name' => 'Noel Carroll',
			'value1' => 429,
			'value2' => 829,
			'value3' => 427,
			'value4' => 826,
			'value5' => 859
            ]
        ];

        DB::table('employees')->insert($employees);
    }
}
