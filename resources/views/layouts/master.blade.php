<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
	<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1">
</head>

<body>
	@yield('content')
</body>
</html>