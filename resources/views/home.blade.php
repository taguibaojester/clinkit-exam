@extends('layouts.master')


@section('content')
<div data-role="page">
  <div data-role="header">
    <center><h1>Employee</h1></center>
  </div> <!-- header -->

  <div class="ui-content">
      <form>
          <input id="filter-for-listview" placeholder="Type to search..." data-type="search">
      </form>
      <div id="demo-borders" data-input="#filter-for-listview" data-filter="true">
      @foreach($employeesByDate as $byDate => $employees)
          <div data-role="collapsible" data-inset="false" data-theme="b">
              <h3>{{$byDate}}</h3>
              <ul data-role="listview">
                  @foreach($employees as $employee)
                    <div data-role="collapsibleset" data-inset="true">
                        <div data-role="collapsible">
                            <h3>{{$employee->name}}</h3>
                            <ul data-role="listview" data-inset="false">
                                <li>{{$employee->value1}}</li>
                                <li>{{$employee->value2}}</li>
                                <li>{{$employee->value3}}</li>
                                <li>{{$employee->value4}}</li>
                                <li>{{$employee->value5}}</li>
                            </ul>
                        </div>
                    </div>                  
                  @endforeach
              </ul>
          </div><!-- /collapsible -->
      @endforeach
      </div>
<br>
  <div data-role="footer">
    <center><h1>End Section</h1></center>
  </div> <!-- footer -->
</div> <!-- page -->
@endsection